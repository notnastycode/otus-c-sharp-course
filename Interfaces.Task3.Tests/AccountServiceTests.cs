using NUnit.Framework;
using Moq;
using System;

namespace Interfaces.Task3.Tests
{
    [TestFixture]
    public class AccountServiceTests
    {
        Mock<IRepository<Account>> _mockRepository;

        [SetUp]
        public void Setup()
        {
            _mockRepository = new Mock<IRepository<Account>>();
        }

        [Test]
        public void Constructor_RepositoryIsNull_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new AccountService(null));
        }

        [Test]
        public void AddAccount_AccountAgeLessThanAdmissibleAge_RepositoryDoesNotCallAdd()
        {
            // Arrange
            var account = new Account("Nastya", "Kononchuk", DateTime.Now);
            AccountService accountService = new AccountService(_mockRepository.Object);

            //Act
            accountService.AddAccount(account);

            //Assert
            _mockRepository.Verify(mock => mock.Add(account), Times.Never);
        }

        [Test]
        public void AddAccount_AccountFileNameIsEmpty_RepositoryDoesNotCallAdd()
        {
            // Arrange
            var account = new Account(" ", "Kononchuk", new System.DateTime(1996, 11, 15));
            AccountService accountService = new AccountService(_mockRepository.Object);

            //Act
            accountService.AddAccount(account);

            //Assert
            _mockRepository.Verify(mock => mock.Add(account), Times.Never);
        }

        [Test]
        public void AddAccount_AccountLastNameIsEmpty_RepositoryDoesNotCallAdd()
        {
            // Arrange
            var account = new Account("Nastya", " ", new System.DateTime(1996, 11, 15));
            AccountService accountService = new AccountService(_mockRepository.Object);

            //Act
            accountService.AddAccount(account);

            //Assert
            _mockRepository.Verify(mock => mock.Add(account), Times.Never);
        }

        [Test]
        public void AddAccount_AccountIsValid_RepositoryDoesNotCallAdd()
        {
            // Arrange
            var account = new Account("Nastya", "Kononchuk", new System.DateTime(1996, 11, 15));
            AccountService accountService = new AccountService(_mockRepository.Object);

            //Act
            accountService.AddAccount(account);

            //Assert
            _mockRepository.Verify(mock => mock.Add(account), Times.Once);
        }
    }
}