using NUnit.Framework;
using System.Linq;
using System.Collections.Generic;
using Moq;
using System;

namespace Interfaces.Task3.Tests
{
    [TestFixture]
    public class RepositoryTests
    {
        Mock<ISerializer<IEnumerable<Account>>> _mockSerializer;
        Mock<IFileWriter> _mockFileWriter;

        [SetUp]
        public void Setup()
        {
            _mockSerializer = new Mock<ISerializer<IEnumerable<Account>>>();
            _mockFileWriter = new Mock<IFileWriter>();
        }

        [Test]
        public void Constructor_SerializerIsNull_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new Repository<Account>(null, _mockFileWriter.Object));
        }

        [Test]
        public void Constructor_FileWriterIsNull_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new Repository<Account>(_mockSerializer.Object,  null));
        }

        [Test]
        public void Add_AddsItemToArray_CollecionCountChanged()
        {
            // Arrange
            Account account = new Account();
            var repository = new Repository<Account>(_mockSerializer.Object, _mockFileWriter.Object);
            var countBeforeAdding = repository.GetAll().Count();

            //Act
            repository.Add(account);

            //Assert
            var countAfterAdding = repository.GetAll().Count();
            Assert.True(countBeforeAdding < countAfterAdding, 
                "Count of collection wasn't changed");
        }

        [Test]
        public void GetAll_ReturnsAllItemsThatWereAdded()
        {
            // Arrange
            List<Account> list = new List<Account>() {new Account("Nastya", "Kononchuk",
                            new System.DateTime(1996, 11, 15)), new Account("Yaroslav", "Matskan",
                            new System.DateTime(2006, 7, 5)) };
            var repository = new Repository<Account>(_mockSerializer.Object, _mockFileWriter.Object);
            foreach (var item in list)
            {
                repository.Add(item);
            }

            //Act
            var all = repository.GetAll();

            //Assert
            Assert.AreEqual(all, list, "GetAll() returned not valid sequel");
        }

        [Test]
        public void GetOne_SearchExistedAccount_ReturnsValidAccount()
        {
            // Arrange
            string searchName = "Yaroslav";
            List<Account> list = new List<Account>() {new Account("Nastya", "Kononchuk",
                            new System.DateTime(1996, 11, 15)), new Account(searchName, "Matskan",
                            new System.DateTime(2006, 7, 5)) };
            var repository = new Repository<Account>(_mockSerializer.Object, _mockFileWriter.Object);
            foreach (var item in list)
            {
                repository.Add(item);
            }

            //Act
            var account = repository.GetOne(x => x.FirstName == searchName);

            //Assert
            Assert.AreEqual(account?.FirstName, searchName, "Accounts don't match");
        }

        [Test]
        public void GetOne_SearchNonExistedAccount_ReturnsNull()
        {
            // Arrange
            string searchName = "Yaroslav";
            List<Account> list = new List<Account>();
            var repository = new Repository<Account>(_mockSerializer.Object, _mockFileWriter.Object);
            foreach (var item in list)
            {
                repository.Add(item);
            }

            //Act
            var account = repository.GetOne(x => x.FirstName == searchName);

            //Assert
            Assert.Null(account, "Account isn't null");
        }

        [Test]
        public void SaveToFile_JsonSerializerIsCalled()
        {
            // Arrange
            string filePath = AppContext.BaseDirectory;
            var repository = new Repository<Account>(_mockSerializer.Object, _mockFileWriter.Object);

            //Act
            repository.SaveToFile(filePath);


            //Assert
            _mockSerializer.Verify(mock => mock.Serialize(It.IsAny<IEnumerable<Account>>()), Times.Once);
        }

        [Test]
        public void SaveToFile_FileWrierIsCalled()
        {
            // Arrange
            string filePath = "C:\\";
            var repository = new Repository<Account>(_mockSerializer.Object, _mockFileWriter.Object);

            //Act
            repository.SaveToFile(filePath);


            //Assert
            _mockFileWriter.Verify(mock => mock.WriteToFile(It.IsAny<string>(), filePath), Times.Once);
        }
    }
}