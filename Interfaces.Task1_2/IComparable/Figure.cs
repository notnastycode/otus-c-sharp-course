﻿using System;

namespace Interfaces.Task1_2
{
    [Serializable]
    public class Figure : IComparable<Figure>
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public string Color { get; set; }

        public int CompareTo(Figure other)
        {
            if (other == null)
            {
                return 1;
            }
            else
            {
                return GetSquare().CompareTo(other.GetSquare());
            }
        }

        public int GetSquare()
        {
            return Width * Height;
        }

        public override string ToString()
        {
            return $"Card with Color: {Color}, Width: {Width}, Height: {Height}";
        }
    }
}
