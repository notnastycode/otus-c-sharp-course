﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Interfaces.Task1_2
{
    public class FigureSorter : ISorter<Figure>
    {
        public IEnumerable<Figure> SortByAscending(IEnumerable<Figure> notSortedItems)
        {
            if (notSortedItems == null)
            {
                throw new ArgumentNullException(nameof(notSortedItems));
            }

            return notSortedItems.OrderBy(card => card);
        }

        public IEnumerable<Figure> SortByDescending(IEnumerable<Figure> notSortedItems)
        {
            if (notSortedItems == null)
            {
                throw new ArgumentNullException(nameof(notSortedItems));
            }

            return notSortedItems.OrderByDescending(card => card);
        }
    }
}
