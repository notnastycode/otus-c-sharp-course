﻿using System.Collections.Generic;

namespace Interfaces.Task1_2
{
    public interface ISorter<T>
    {
        IEnumerable<T> SortByAscending(IEnumerable<T> notSortedItems);

        IEnumerable<T> SortByDescending(IEnumerable<T> notSortedItems);
    }
}
