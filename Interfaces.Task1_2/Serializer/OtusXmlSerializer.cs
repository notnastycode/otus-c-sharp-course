﻿using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using System.IO;
using System.Xml;

namespace Interfaces.Task1_2
{
    public class OtusXmlSerializer<T> : ISerializer<T>
    {
        private readonly IExtendedXmlSerializer _serializer;
        public OtusXmlSerializer()
        {
            _serializer = new ConfigurationContainer().UseAutoFormatting()
                                                    .UseOptimizedNamespaces()
                                                    .EnableImplicitTyping(typeof(T))
                                                    .Emit(EmitBehaviors.WhenAssigned)
                                                    .Create();
        }

        public T Deserialize(Stream stream)
        {
            return _serializer.Deserialize<T>(stream);
        }

        public string Serialize(T item)
        {
            return _serializer.Serialize(new XmlWriterSettings { Indent = true }, item);
        }
    }
}