﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Interfaces.Task1_2
{
    public interface ISerializer<T>
    {
        string Serialize(T item);
        T Deserialize(Stream stream);
    }
}
