﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Interfaces.Task1_2
{
    class Program
    {
        private const string FileName = "Figures.xml";

        static void Main(string[] args)
        {
            OtusXmlSerializer<Figure[]> otusXmlSerializer = new OtusXmlSerializer<Figure[]>();
            using (Stream stream = new FileStream(FileName, FileMode.OpenOrCreate))
            {
                using (var streamReader = new OtusStreamReader<Figure>(stream, otusXmlSerializer))
                {
                    foreach (var figure in streamReader)
                    {
                        Console.WriteLine(figure.ToString());
                    }

                    FigureSorter figureSorter = new FigureSorter();
                    var sortedByAscending = figureSorter.SortByAscending(streamReader);
                    Console.WriteLine(Environment.NewLine + "Sorted by Ascending:");
                    foreach (var figure in sortedByAscending)
                    {
                        Console.WriteLine(figure.ToString());
                    }

                    var sortedByDescending = figureSorter.SortByDescending(streamReader);
                    Console.WriteLine(Environment.NewLine + "Sorted by Descending:");
                    foreach (var figure in sortedByDescending)
                    {
                        Console.WriteLine(figure.ToString());
                    }

                    var algorithm = new Algorithm<Figure>(figureSorter);
                    Figure searchCard = new Figure() { Height = 5, Width = 3 };
                    var findIndex = algorithm.BinarySearchForNotSortedItems(streamReader, searchCard);
                    Console.WriteLine(Environment.NewLine +  $"Index of {searchCard} with the same square: {findIndex}");
                }
            }
        }
    }
}
