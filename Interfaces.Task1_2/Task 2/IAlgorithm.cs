﻿using System;
using System.Collections.Generic;

namespace Interfaces.Task1_2
{
    public  interface IAlgorithm<T> where T : IComparable<T>
    {
        int BinarySearchForNotSortedItems(IEnumerable<T> notSortedItems, T searchItem);
    }
}
