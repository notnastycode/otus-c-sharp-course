﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Interfaces.Task1_2
{
    public class Algorithm<T> : IAlgorithm<T> where T: IComparable<T>
    {
        private ISorter<T> _sorter;
        public Algorithm(ISorter<T> sorter)
        {
            if (sorter == null)
            {
                throw new ArgumentNullException(nameof(sorter));
            }

            _sorter = sorter;
        }

        public int BinarySearchForNotSortedItems(IEnumerable<T> notSortedItems, T searchItem)
        {
            if (notSortedItems == null)
            {
                throw new ArgumentNullException(nameof(notSortedItems));
            }

            var sortedItems = _sorter.SortByAscending(notSortedItems);
            int leftestIndex = 0;
            int rightestIndex = sortedItems.Count() - 1;
            while (rightestIndex > leftestIndex)
            {
                int currentIndex = (leftestIndex + rightestIndex) / 2;
                T currentVaule = sortedItems.ElementAt(currentIndex);
                int? compareResult = currentVaule?.CompareTo(searchItem);
                if (compareResult == 0 || (currentVaule == null && searchItem == null))
                {
                    return currentIndex;
                }

                if (compareResult > 0)
                {
                    rightestIndex = currentIndex - 1;
                }
                else
                {
                    leftestIndex = currentIndex + 1;
                }
            }

            return -1;
        }
    }
}
