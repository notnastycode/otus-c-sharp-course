﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace Interfaces.Task1_2
{
    public class OtusStreamReader<T> : IEnumerable<T>, IDisposable
    {
        private ISerializer<T[]> _serializer;
        private Stream _stream;
        private T[] _arrayOfDeserialized;
        private bool _disposedValue = false;
        

        public OtusStreamReader(Stream stream, ISerializer<T[]> serializer)
        {
            if (serializer == null)
            {
                throw new ArgumentNullException(nameof(serializer));
            }

            if (stream == null)
            {
                throw new ArgumentNullException(nameof(serializer));
            }

            _serializer = serializer;
            _stream = stream;
        }

        private T[] GetDeserializedArray()
        {
            if (_arrayOfDeserialized == null)
            {
                _arrayOfDeserialized = _serializer.Deserialize(_stream);
            }
            return _arrayOfDeserialized;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    _serializer = null;
                    _stream = null;
                    _arrayOfDeserialized = null;
                }
            }

            _disposedValue = true;
        }

        public IEnumerator<T> GetEnumerator()
        {
            var array = GetDeserializedArray();
            for (int i = 0; i < array.Length; i++)
            {
                yield return array[i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            yield return this.GetEnumerator();
        }

        ~OtusStreamReader()
        {
            Dispose(false);
        }
    }
}