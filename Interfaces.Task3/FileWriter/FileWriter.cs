﻿using System.IO;
using System.Text;

namespace Interfaces.Task3
{
    public class FileWriter : IFileWriter
    {
        public void WriteToFile(string text, string filePath)
        {
            File.WriteAllText(filePath, text, Encoding.UTF8);
        }
    }
}
