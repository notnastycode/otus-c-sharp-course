﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Task3
{
    public interface IFileWriter
    {
        void WriteToFile(string text, string filePath);
    }
}
