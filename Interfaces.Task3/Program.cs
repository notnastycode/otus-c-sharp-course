﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Interfaces.Task3
{
    class Program
    {
        private const string FileName = "SavedFile.xml";

        static void Main(string[] args)
        {
            FileWriter fileWriter = new FileWriter();
            var myJsonSerializer = new MyJsonSerializer<IEnumerable<Account>>();
            var repository = new Repository<Account>(myJsonSerializer, fileWriter);
            var account = new Account("Nastya", "Kononchuk", new DateTime(1996, 11, 15));
            repository.Add(account);
            repository.SaveToFile(Path.Combine(Directory.GetCurrentDirectory(), FileName));
        }
    }
}
