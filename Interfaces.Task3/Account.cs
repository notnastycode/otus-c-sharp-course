﻿using System;

namespace Interfaces.Task3
{
    public class Account
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        
        public Account() { }

        public Account(string fileName, string lastName, DateTime birthDate)
        {
            FirstName = fileName;
            LastName = lastName;
            BirthDate = birthDate;
        }

        public int GetAge()
        {
            var zeroTime = new DateTime(1, 1, 1);
            var span = DateTime.Now.Subtract(BirthDate);
            span.Subtract(TimeSpan.FromDays(1));
            return (new DateTime(1, 1, 1) + span).Year - 1;
        }
    }
}
