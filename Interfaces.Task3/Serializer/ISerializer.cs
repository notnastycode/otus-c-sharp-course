﻿namespace Interfaces.Task3
{
    public interface ISerializer<T>
    {
        string Serialize(T item);
    }
}
