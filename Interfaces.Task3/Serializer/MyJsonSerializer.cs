﻿using System.Text.Json;

namespace Interfaces.Task3
{
    public class MyJsonSerializer<T> : ISerializer<T>
    {
        public string Serialize(T item)
        {
            return JsonSerializer.Serialize(item);
        }
    }
}