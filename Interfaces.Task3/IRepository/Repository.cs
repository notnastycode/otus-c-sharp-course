﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Interfaces.Task3
{
    public class Repository<T> : IRepository<T>
    {
        private List<T> _array = new List<T>();
        private ISerializer<IEnumerable<T>> _jsonSerializer;
        private IFileWriter _fileWriter;

        public Repository(ISerializer<IEnumerable<T>> jsonSerializer, IFileWriter fileWriter)
        {
            if (jsonSerializer == null)
            {
                throw new ArgumentNullException(nameof(jsonSerializer));
            }

            if (fileWriter == null)
            {
                throw new ArgumentNullException(nameof(jsonSerializer));
            }

            _jsonSerializer = jsonSerializer;
            _fileWriter = fileWriter;
        }

        public void Add(T item)
        {
            _array.Add(item);
        }

        public IEnumerable<T> GetAll()
        {
            foreach (var item in _array)
            {
                yield return item;
            }
        }

        public T GetOne(Func<T, bool> predicate)
        {
            return _array.FirstOrDefault(predicate);
        }

        public void SaveToFile(string filePath)
        {
          string jsonString = _jsonSerializer.Serialize(_array);
          _fileWriter.WriteToFile(jsonString, filePath);
        }
    }
}
