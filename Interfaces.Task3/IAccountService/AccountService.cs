﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Task3
{
    public class AccountService : IAccountService
    {
        private IRepository<Account> _repository;
        private const int AdmissinleAge = 18;

        public AccountService(IRepository<Account> repository)
        {
            if (repository == null)
            {
                throw new ArgumentNullException(nameof(repository));
            }

            _repository = repository;
        }

        public void AddAccount(Account account)
        {
            var isValid = ValidateAccount(account);
            if (isValid)
            {
                _repository.Add(account);
            }
        }

        private bool ValidateAccount(Account account)
        {
            return account.GetAge() >= AdmissinleAge && !string.IsNullOrWhiteSpace(account.FirstName)
                && !string.IsNullOrWhiteSpace(account.LastName);
        }
    }
}
