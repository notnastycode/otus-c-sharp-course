﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Task3
{
    public interface IAccountService
    {
        void AddAccount(Account account);
    }
}
